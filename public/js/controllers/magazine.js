(function(){
    'use strict';

    angular.module('Tree')
        .controller('MagazineCtrl', MagazineCtrl);

    MagazineCtrl.$inject = ['$scope', 'Magazines'];
    function MagazineCtrl($scope, Magazines){
        var vm = this;
        vm.magazines = [
            {topic: 'One'},
            {topic: 'Two'},
            {topic: 'Three'},
            {topic: 'Four'},
            {topic: 'Five'}
        ];
    }
})();