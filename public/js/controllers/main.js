(function(){
    'use strict';

    angular.module('Tree')
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$scope'];
    function MainCtrl($scope){
        var vm = this;
        vm.test = 'MainCtrl';
    }
})();
