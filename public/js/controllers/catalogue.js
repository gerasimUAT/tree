(function(){
    'use strict';

    var Tree = angular.module('Tree');
    Tree.controller('CatalogueCtrl', CatalogueCtrl);

    CatalogueCtrl.$inject = ['$scope', 'Catalogues'];
    function CatalogueCtrl($scope, Catalogues){
        var vm = this;
        vm.delete = deleteCatalogue;
        vm.change = change;
        vm.edit = edit;
        vm.add = add;
        vm.tree = [{name: "Root", id: 0, parent: null, nodes: []}];

        function add(data) {
            var post = data.nodes.length + 1;
            var newName = data.name + '-' + post;
            data.nodes.push({name: newName, nodes: []});
        }

        function deleteCatalogue(data) {
            data.nodes = [];
        }

        function change(data){
            //
        }

        function edit(data){
            //
        }

        Catalogues.get(function (data) {
            console.warn(data);
        });

        //todo from root scope
        $('body').on('show.bs.popover', function(){
            $('.magazines').addClass('blur');
        });
        $('body').on('hide.bs.popover', function(){
            $('.magazines').removeClass('blur');
        })

    }

    Tree.directive('popover', function(){
        return function(scope, elem, attrs) {
            $(elem).on('click', attrs.popover, function () {
                $(elem)
                    .popover({
                        content: '<input placeholder="title..." class="form-control">' +
                                 '<div class="popoverControl">' +
                                     '<button class="btn btn-default btn-xs btn-block">Cancel</button>' +
                                     '<button class="btn btn-success btn-xs btn-block" ng-click="add(data)">Save</button>' +
                                 '</div>',
                        title: 'Add new catalogue',
                        html: true
                    })
                    .popover('show');
            });
        }
    })

})();