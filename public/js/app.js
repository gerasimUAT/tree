(function(){
    'use strict';

    angular
        .module('Tree', [
            'ngResource',
            'LocalStorageModule',
            'cataloguesService',
            'magazinesService'
        ], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        })
        .config(['localStorageServiceProvider', function(localStorageServiceProvider){
            localStorageServiceProvider.setPrefix('tree');
        }])
        .filter('nl2br', function () {
            return function(text) {
                if(typeof text !== 'string'){ return ''; }
                return text.replace(/\n/g, '<br/>');
            }
        });

})();
