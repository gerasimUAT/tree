(function(){
    'use strict';

    angular
        .module('cataloguesService', ['ngResource'])
        .factory('Catalogues', Catalogues);

    Catalogues.$inject = ['$resource'];
    function Catalogues($resource){
        return $resource('/catalogues/:id', {id: '@id'}, {});
    }
})();
