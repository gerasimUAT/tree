(function(){
    'use strict';

    angular
        .module('magazinesService', ['ngResource'])
        .factory('Magazines', Magazines);

    Magazines.$inject = ['$resource'];
    function Magazines($resource){
        return $resource('/magazines/:id', {id: '@id'}, {});
    }
})();