@extends('layouts.main')

@section('content')
    <div class="row header">
        <div class="col-xs-9">
            <h2>
                <span class="glyphicon glyphicon-tree-deciduous"></span>
                Tree
            </h2>
        </div>
        <div class="col-xs-3">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        Search
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4" ng-controller="CatalogueCtrl as cv">
            <div class="catalogues">
                <script type="text/ng-template" id="treeItemRender.html">
                    <div class="item" popover=".addCatalogue">
                        <div class="pull-left" ng-click="cv.changeCatalogue(data)">
                            <% data.name %>
                        </div>
                        <div class="pull-right">
                            <button class="btn btn-success btn-xs addCatalogue">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                            <button ng-click="cv.edit(data)"  class="btn btn-warning btn-xs">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </button>
                            <button ng-click="cv.delete(data)" ng-show="data.nodes.length > 0" class="btn btn-danger btn-xs">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <ul>
                        <li ng-repeat="data in data.nodes" ng-include="'treeItemRender.html'"></li>
                    </ul>
                </script>
                <ul class="firstBlock">
                    <li ng-repeat="data in cv.tree" ng-include="'treeItemRender.html'"></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-8" ng-controller="MagazineCtrl as mgv">
            <div class="magazines">
                <ul class="firstBlock">
                    <li ng-repeat="magazine in mgv.magazines">
                        <% magazine.topic %>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
