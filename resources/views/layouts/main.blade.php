<!DOCTYPE html>
<html lang="en" ng-app="Tree">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tree</title>
        <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('/components/toastr/toastr.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/main.css') }}" rel="stylesheet">
    </head>
    <body ng-controller="MainCtrl as mv">
        <div class="container-fluid">
            @yield('content')
        </div>
        <script src="{{ asset('/components/angular/angular.js') }}"></script>
        <script src="{{ asset('/components/angular-resource/angular-resource.js') }}"></script>
        <script src="{{ asset('/components/angular-route/angular-route.js') }}"></script>
        <script src="{{ asset('/components/angular-local-storage/dist/angular-local-storage.js') }}"></script>
        <script src="{{ asset('/components/jquery/dist/jquery.js') }}"></script>
        <script src="{{ asset('/components/bootstrap/dist/js/bootstrap.js') }}"></script>
        <script src="{{ asset('/components/moment/moment.js') }}"></script>
        <script src="{{ asset('/components/toastr/toastr.js') }}"></script>
        <script src="{{ asset('/js/app.js') }}"></script>
        <script src="{{ asset('/js/services/catalogues.js') }}"></script>
        <script src="{{ asset('/js/services/magazines.js') }}"></script>
        <script src="{{ asset('/js/controllers/main.js') }}"></script>
        <script src="{{ asset('/js/controllers/catalogue.js') }}"></script>
        <script src="{{ asset('/js/controllers/magazine.js') }}"></script>
    </body>
</html>